#Configura los fuentes

open_project -reset deteccion_parasitos
set_top contar_huevos

add_files "src/clasificacion.cpp" -cflags "-Ixfopencv/include -D__SDSVHLS__"
add_files "src/clasificacion.h"
add_files "src/config.h"
add_files "src/blob.h"
add_files "src/camino.h"
add_files "src/constantes.h"
add_files "src/contador_huevos.cpp" -cflags "-Ixfopencv/include -D__SDSVHLS__"
add_files "src/preprocesamiento.cpp" -cflags "-Ixfopencv/include -D__SDSVHLS__"
add_files "src/preprocesamiento.h"
add_files "src/segmentacion.cpp" -cflags "-Ixfopencv/include -D__SDSVHLS__"
add_files "src/segmentacion.h"
add_files "src/tipos.h"
add_files "src/xf_headers.h"
add_files -tb "test/contador_huevos_tb.cpp" -cflags "-Ixfopencv/include -D__SDSVHLS__ -std=c++14 -Wno-unknown-pragmas"
add_files -tb "test/resultados_esperados.h" -cflags "-Wno-unknown-pragmas"


#Configura las soluciones

set all_solution [list pynq_dataflow_unroll_inline_pipelining pynq_dataflow_unroll_inline pynq_dataflow_unroll_pipelining pynq_dataflow_y_unroll pynq_v1 pynq_v2 pynq_v3_dataflow ultra96v2_dataflow ultra96v2_dataflow_y_unroll ultra96v2_v1 ultra96v2_dataflow_unroll_inline_pipelining ]

set all_part [list xc7z020-clg400-1 xc7z020-clg400-1 xc7z020-clg400-1 xc7z020-clg400-1 xc7z020-clg400-1 xc7z020-clg400-1 xc7z020-clg400-1 xczu3eg-sbva484-1-i xczu3eg-sbva484-1-i xczu3eg-sbva484-1-i xczu3eg-sbva484-1-i]

set all_directives [list "deteccion_parasitos/pynq_dataflow_unroll_inline_pipelining/directives.tcl" "deteccion_parasitos/pynq_dataflow_unroll_inline/directives.tcl" "deteccion_parasitos/pynq_dataflow_unroll_pipelining/directives.tcl" "deteccion_parasitos/pynq_dataflow_y_unroll/directives.tcl" "deteccion_parasitos/pynq_v1/directives.tcl" "deteccion_parasitos/pynq_v2/directives.tcl" "deteccion_parasitos/pynq_v3_dataflow/directives.tcl" "deteccion_parasitos/ultra96v2_dataflow/directives.tcl" "deteccion_parasitos/ultra96v2_dataflow_y_unroll/directives.tcl" "deteccion_parasitos/ultra96v2_v1/directives.tcl" "deteccion_parasitos/ultra96v2_dataflow_unroll_inline_pipelining/directives.tcl"]

foreach solution $all_solution part $all_part directives $all_directives {

    open_solution -reset $solution
    set_part $part
    create_clock -period 10 -name default
    config_sdx -target none
    config_export -vivado_optimization_level 2 -vivado_phys_opt place -vivado_report_level 0
    set_clock_uncertainty 12.5%

    #Para este set de directivas en particular, optimiza el esfuerzo por reducir área. Como puede producir fallos en la cosimulación, es importante chequear si aplica para cada caso.
    if { $solution eq "ultra96v2_dataflow_unroll_inline_pipelining" } {
        config_bind -effort high
    }

    source $directives

}

exit
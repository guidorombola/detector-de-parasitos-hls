# Detector de parásitos HLS

Trabajo final de Ingeniería en Computación - Algoritmo sintetizable en hardware para la detección de parásitos bovinos.

El desarrollo se realiza mediante la herramienta de síntesis de alto nivel Vivado HLS 2019.2 y es apto para FPGA.

## Release

El resultado final de este desarrollo produce los dos IP Cores disponibles en:
https://gitlab.com/guidorombola/detector-de-parasitos-hls/-/releases/v1.0.0 uno para PNYQ-Z1 y otro para ULTRA96V2.

## Configuración del entorno de desarrollo

### Requisitos previos:

* Sistemas operativos: Windows 10 / Ubuntu 18.04.2.

* [Vivado HLS 2019.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive.html) (Sección Vivado Design Suite - HLx Editions)

### Aclaraciones:
 
* Se recomienda la utilización en Windows por no requerir dependencias adicionales en la ejecución.

* Si bien es posible que corra en una versión más actualizada de Ubuntu, se recomienda esta versión porque es la que Xilinx documenta tener soporte en Vivado HLS 2019.2. Para instructivo de instalación en este sistema operativo, ver sección 'Instalación en Ubuntu 18.04.2'

### Instalación Ubuntu 18.04.2

* Será necesario instalar los siguientes paquetes previamente a la instalación de Vivado HLS:
    
    * build-essential:
       
            sudo apt install build-essential
    * libopencl:

            sudo apt install ocl-icd-opencl-dev

    * libpng12:
    
            wget -q -O /tmp/libpng12.deb http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1_amd64.deb && sudo dpkg -i /tmp/libpng12 .deb && rm /tmp/libpng12.deb

    * libjpeg62:

            sudo apt install libjpeg62-dev


### Configuración del proyecto:
* Asegurarse de que vivado_hls está en el PATH. Caso contrario agregar al PATH el directorio `bin` ubicado en la ruta de instalación de Vivado HLS.
* Descargar y configurar en Vivado HLS los archivos de las placas:
    * [Ultra96V2](https://github.com/Avnet/bdf) (hay que descargar el repositorio, pero nos interesa únicamente el directorio `ultra96v2`) y [Pynq-Z1](https://github.com/cathalmccabe/pynq-z1_board_files/raw/master/pynq-z1.zip)
    * Mover los directorios `ultra96v2` y `pynq-z1` a:
            
            ruta_instalacion_vivado/data/boards/board_files

* Ejecutar el siguiente comando en la raiz del proyecto:
    
        vivado_hls generar_proyecto.tcl
    Esto generará la estructura de proyecto necesaria para Vivado_HLS. Las soluciones para las placas destino con sus respectivas directivas se encuentran en el directorio `deteccion_parasitos`.
* Abrir Vivado HLS, abrir un nuevo proyecto, y seleccionar el directorio `deteccion_parasitos`.
* A partir de este punto se pueden ejecutar las tareas deseadas, como las simulaciones de alto nivel, y las síntesis.


## Ejecución rápida

Se provee un entorno de ejecución basado en un container para poder verificar el funcionamiento de la aplicación sin necesidad de configurar el entorno de desarrollo, y evitando los problemas por dependencias del sistema operativo. Este entorno provee la posibilidad de ejecutar el software, pero no permite compilar una solución.

### Requisitos previos
* Docker

### Contenido

Se puede encontrar este entorno en el directorio `entorno-ejecución-containerizado` en la raiz del proyecto. Dentro del mismo encontramos los siguientes items:
* Directorio `app`: contiene la última versión compilada de la aplicación. En caso de querer probar otra versión, se puede reemplazar por el ejecutable que se desee, manteniendo el mismo nombre.
* Directorio `ìmagenes_prueba`: contiene las imágenes que servirán de entrada para la aplicación.
* Directorio `lib`: contiene las bibliotecas compartidas necesarias para el funcionamiento de la aplicación.
* Archivo `Dockerfile`: define la receta con la que se construirá la imagen utilizada para levantar el contenedor que ejecuta la aplicación.
* Archivos ejecutables `ejecutar.bat` y `ejecutar.sh`: permiten ejecutar la aplicación en Windows y Ubuntu respectivamente. 

### Utilización

1. Pararse en el directorio `entorno-ejecución-containerizado`:

2. Ejecutar la versión del ejecutable que corresponda con tu sistema operativo:

* En Windows:  

        ejecutar.bat

* En Ubuntu:
        
        
  * Otorgar permisos de ejecución a la aplicacion y al script de puesta en marcha del contenedor. Estando en el directorio `entorno-ejecucion-containerizado`:
        
        chmod +x ejecutar.sh
        cd app
        chmod +x csim  
        
  * Ejecutar aplicación
        
        ./ejecutar.sh

3. La salida de la ejecución será el conjunto de imágenes del directorio `imagenes_prueba` con los objetos identificados como huevos marcados con un recuadro. Estos resultados se encuentran en el directorio `app`.

## Uso de las características principales del entorno de desarrollo de Vivado HLS y del proyecto


### Implementaciones alternativas

El producto cuenta con la implementación apta para síntesis (su resultado arroja únicamente la cantidad de huevos encontrados), y la implementación pensada para el desarrollo (lo anterior, más las imágenes resaltadas con sus detecciones). Para conmutar entre un modo u otro, definir o comentar la constante `MODO_SIMULACION` en el archivo `constantes.h`.

### Seleccion de una solución

Habiendo preparado la estructura del proyecto de acuerdo a lo especificado en la sección `Configuración del proyecto`, será necesario seleccionar una de las soluciones haciendo doble click desde el explorador  para poder ejecutar las tareas que se explican a continuación (`pynq_dataflow_unroll_inline_pipelining` y `ultra96v2_dataflow_unroll_inline_pipelining` son las soluciones finales de este trabajo, una por cada kit de desarrollo).

### Simulación de software

* Opciones `Project > Run C simulation`
* Seleccionar ruta desde donde tomar las imágenes
* Ejecutar
* Las imágenes de salida se almacenarán en el directorio de la solución, subdirectorio `csim/build`        

### Síntesis (conversión código C a RTL)

* Opciones `Solution > Run C Synthesis > Active Solution`
* Al finalizar el proceso, se mostrará el reporte con los resultados de uso de recursos y tiempos. Además, se guardará el reporte en el directorio de la solución, subdirectorio `syn/report`, archivo `contar_huevos_csynth.rpt`. Los diseños RTL generados estarán disponibles en los subdirectorios `syn/vhdl` y `syn/verilog` para los lenguajes de descripción de hardware VHDL y Verilog respectivamente.

### Co-simulación

* Opciones `Solution > Run C/RTL Cosimulation`
* Seleccionar ruta desde donde tomar las imágenes
* En Ubuntu 18.04.2, establecer la opción 'dump trace' en 'all'.
* Ejecutar
* Al finalizar, se mostrará un aviso indicando si la cosimulación fue exitosa o fallida.

### Exportar IP Core

El ultimo paso para finalizar un desarrollo consiste en exportar el IP core para poder sintetizarlo en el hardware. Para realizar esta tarea:

* Opciones `Solution > Export RTL`
* Dejar las opciones por defecto y presionar Ok
* Al finalizar el proceso, se guardará el IP core en el directorio de la solución, subdirectorio `impl/ip`. Este IP core es el archivo con extensión .zip, y es el punto de partida para síntesis en FPGA utilizando otras herramientas.

Aclaración: a partir del año 2022 se manifestó una falla que impide exportar un IP core, y tiene que ver con el manejo de fechas del entorno de desarrollo. Para poder completar la acción hay dos alternativas, cambiar la fecha del sistema a 2021 o aplicar el siguiente [parche](https://support.xilinx.com/s/article/76960?language=en_US) siguiendo las instrucciones del README de la descarga.


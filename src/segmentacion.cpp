#include "segmentacion.h"
#include "ap_fixed.h"

void inicializar_caminos(Camino (&caminosFilaActual)[CANTIDAD_MAXIMA_CAMINOS], Camino (&caminosFilaAnterior)[CANTIDAD_MAXIMA_CAMINOS]){
	inicializar_caminos_label0:for(int i=0; i<CANTIDAD_MAXIMA_CAMINOS; i++) {
		caminosFilaActual[i].inicio = 0;
		caminosFilaActual[i].fin = 0;
		caminosFilaActual[i].referenciaABlob = 0;
		caminosFilaAnterior[i].inicio = 0;
		caminosFilaAnterior[i].fin = 0;
		caminosFilaAnterior[i].referenciaABlob = 0;
	}
}

void incrementar_momentos(Camino (&caminoFilaActual), Blob &blob, TIPO_COORDENADA fila){

	ap_uint<10> n = caminoFilaActual.fin - caminoFilaActual.inicio + 1;
	blob.momento10 += ((caminoFilaActual.fin * (caminoFilaActual.fin + 1)) - ((caminoFilaActual.inicio - 1) * (caminoFilaActual.inicio))) / 2 ;
	blob.momento01 += (n * fila);
	blob.momento11 += (fila * (((caminoFilaActual.fin * (caminoFilaActual.fin + 1)) - ((caminoFilaActual.inicio - 1) * (caminoFilaActual.inicio))) / 2));
	blob.momento02 += (fila * fila * n);
	blob.momento20 += ((caminoFilaActual.fin * (caminoFilaActual.fin + 1) * ((2*caminoFilaActual.fin) + 1)) - ((caminoFilaActual.inicio - 1) * (caminoFilaActual.inicio) * (2 * (caminoFilaActual.inicio - 1) + 1))) / 6;

}

void combinar_momentos(Blob &blobFilaActual, Blob &blobFilaAnterior){
	blobFilaActual.momento10 += blobFilaAnterior.momento10;
	blobFilaActual.momento01 += blobFilaAnterior.momento01;
	blobFilaActual.momento11 += blobFilaAnterior.momento11;
	blobFilaActual.momento02 += blobFilaAnterior.momento02;
	blobFilaActual.momento20 += blobFilaAnterior.momento20;
}

void actualizar_caracteristicas(Blob &blob, Camino &camino, TIPO_COORDENADA fila) {
	blob.area += camino.fin - camino.inicio + 1;
	incrementar_momentos(camino, blob, fila);
	blob.filaMaxima = fila;
	if (camino.inicio < blob.columnaMinima){
		blob.columnaMinima = camino.inicio;
	}
	if (camino.fin > blob.columnaMaxima) {
		blob.columnaMaxima = camino.fin;
	}

}

void combinar_caracterisiticas(Blob &blobNuevo, Blob &blobPrevio, Camino &caminoFilaActual) {
	blobNuevo.area += blobPrevio.area;

	combinar_momentos(blobNuevo, blobPrevio);

	blobPrevio.referenciaANuevoBlob = caminoFilaActual.referenciaABlob;
	blobPrevio.esValido =  false;

	if (caminoFilaActual.fin < blobPrevio.columnaMaxima) {
		blobNuevo.columnaMaxima = blobPrevio.columnaMaxima;
	}
	if (blobPrevio.filaMinima < blobNuevo.filaMinima) {
		blobNuevo.filaMinima = blobPrevio.filaMinima;
	}
	if (blobPrevio.columnaMinima < blobPrevio.columnaMinima) {
		blobNuevo.columnaMinima = blobPrevio.columnaMinima;
	}
	if (blobPrevio.columnaMaxima > blobNuevo.columnaMaxima) {
		blobNuevo.columnaMaxima = blobPrevio.columnaMaxima;
	}

}


bool hay_solapamiento_entre_filas_consecutivas(Camino &caminoFilaAnterior, Camino &caminoFilaActual){
	return ((caminoFilaAnterior.inicio <= caminoFilaActual.inicio) && (caminoFilaAnterior.fin >= caminoFilaActual.inicio)) ||
			((caminoFilaAnterior.inicio <= caminoFilaActual.fin) && (caminoFilaAnterior.fin >= caminoFilaActual.fin)) ||
			((caminoFilaAnterior.inicio >= caminoFilaActual.inicio) && (caminoFilaAnterior.fin <= caminoFilaActual.fin));
}

Blob crear_blob(Camino &camino, TIPO_COORDENADA fila) {
	Blob nuevoBlob;
	nuevoBlob.esValido = true;
	nuevoBlob.area = camino.fin - camino.inicio + 1;
	nuevoBlob.filaMinima = fila;
	nuevoBlob.filaMaxima = fila;
	nuevoBlob.columnaMinima = camino.inicio;
	nuevoBlob.columnaMaxima = camino.fin;
	nuevoBlob.momento10 = 0;
	nuevoBlob.momento01 = 0;
	nuevoBlob.momento11 = 0;
	nuevoBlob.momento02 = 0;
	nuevoBlob.momento20 = 0;
	return nuevoBlob;
}

void verificar_solapamiento(TIPO_REFERENCIA_A_BLOB i, Camino (&caminos)[CANTIDAD_MAXIMA_CAMINOS], Camino & camino, TIPO_COORDENADA fila, Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS], TIPO_REFERENCIA_A_BLOB & contadorBlobs){
	bool huboSolapamientoEntreCaminoActualYAlgunCaminoDeLaFilaAnterior = false;

	verificar_solapamiento_label1:for(TIPO_REFERENCIA_A_BLOB c = 0; c < i; c++){
		if (hay_solapamiento_entre_filas_consecutivas(caminos[c], camino)) {

			TIPO_REFERENCIA_A_BLOB referenciaABlobFilaAnterior = caminos[c].referenciaABlob;
			Blob &blobAnterior = blobs[referenciaABlobFilaAnterior];

			if (!huboSolapamientoEntreCaminoActualYAlgunCaminoDeLaFilaAnterior){

				if (blobAnterior.esValido){
					camino.referenciaABlob = referenciaABlobFilaAnterior;
				} else {
					camino.referenciaABlob = blobAnterior.referenciaANuevoBlob;
				}

				actualizar_caracteristicas(blobAnterior, camino, fila);

				huboSolapamientoEntreCaminoActualYAlgunCaminoDeLaFilaAnterior = true;

			} else {

				if ((camino.referenciaABlob != referenciaABlobFilaAnterior) && (blobAnterior.esValido)){

					combinar_caracterisiticas(blobs[camino.referenciaABlob], blobAnterior, camino);
					caminos[c].referenciaABlob = camino.referenciaABlob;

				}
			}

		}

	}

	if (!huboSolapamientoEntreCaminoActualYAlgunCaminoDeLaFilaAnterior){

		camino.referenciaABlob = contadorBlobs;
		blobs[contadorBlobs] = crear_blob(camino, fila);
		incrementar_momentos(camino, blobs[contadorBlobs], fila);

		contadorBlobs++;
	}

}

void buscar_caminos_en_fila(xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ> & src, Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS], TIPO_REFERENCIA_A_BLOB & contadorBlobs, Camino (&caminosFilaActual)[CANTIDAD_MAXIMA_CAMINOS], Camino (&caminosFilaAnterior)[CANTIDAD_MAXIMA_CAMINOS], TIPO_COORDENADA fila, TIPO_REFERENCIA_A_CAMINO &cantidadCaminosFilaActual, TIPO_REFERENCIA_A_CAMINO &cantidadCaminosFilaAnterior) {

	bool caminoEnProgreso = false;

	cantidadCaminosFilaActual = 0;
	buscar_caminos_en_fila_label1:for(TIPO_COORDENADA i = 0; i < ANCHO; i++ ){

		unsigned char intensidadPixel = src.data[fila*ANCHO+i];

		if (!caminoEnProgreso && intensidadPixel == 255){

			caminoEnProgreso = true;
			caminosFilaActual[cantidadCaminosFilaActual].inicio = i;

		} else if(caminoEnProgreso && intensidadPixel == 0){

			caminoEnProgreso = false;
			caminosFilaActual[cantidadCaminosFilaActual].fin = i - 1;
			cantidadCaminosFilaActual++;
		}

	}
	if (caminoEnProgreso){
		caminosFilaActual[cantidadCaminosFilaActual].fin = ANCHO -1;
		caminoEnProgreso = false;
		cantidadCaminosFilaActual++;
	}

	buscar_caminos_en_fila_label2:for(TIPO_REFERENCIA_A_CAMINO i=0; i<cantidadCaminosFilaActual; i++){
		verificar_solapamiento(cantidadCaminosFilaAnterior, caminosFilaAnterior, caminosFilaActual[i], fila, blobs, contadorBlobs);
	}
}


void detectar_blobs(xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ> & src, Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS], TIPO_REFERENCIA_A_BLOB & contadorBlobs) {

	TIPO_REFERENCIA_A_CAMINO cantidad_caminos_fila_actual = 0;
	TIPO_REFERENCIA_A_CAMINO cantidad_caminos_fila_anterior = 0;

	Camino caminosFilaActual[CANTIDAD_MAXIMA_CAMINOS];
	Camino caminosFilaAnterior[CANTIDAD_MAXIMA_CAMINOS];

	inicializar_caminos(caminosFilaActual, caminosFilaAnterior);

	detectar_blobs_label2:for(TIPO_COORDENADA j = 0; j < (ALTURA); j++ ){

		buscar_caminos_en_fila(src, blobs, contadorBlobs, caminosFilaActual, caminosFilaAnterior, j, cantidad_caminos_fila_actual, cantidad_caminos_fila_anterior);

		cantidad_caminos_fila_anterior = cantidad_caminos_fila_actual;

		detectar_blobs_label0:for(int i=0; i<CANTIDAD_MAXIMA_CAMINOS; i++){
			caminosFilaAnterior[i] = caminosFilaActual[i];
		}

	}

}

void segmentacion_imagen(
		xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _src,
		Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS],
		TIPO_REFERENCIA_A_BLOB &contadorDeBlobs){

	detectar_blobs(_src, blobs, contadorDeBlobs);
}

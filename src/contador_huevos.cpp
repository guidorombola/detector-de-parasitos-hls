#include <string.h>
#include <cmath>
#include "config.h"
#include "tipos.h"
#include "blob.h"
#include "preprocesamiento.h"
#include "segmentacion.h"
#include "clasificacion.h"


#ifdef MODO_SIMULACION
int contar_huevos(TIPO_STREAM_DATOS& _src, xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _dst, Blob (&huevos)[CANTIDAD_MAXIMA_BLOBS]) {

	Blob blobs[CANTIDAD_MAXIMA_BLOBS];
	TIPO_REFERENCIA_A_BLOB contadorDeBlobs = 0;
	TIPO_REFERENCIA_A_BLOB huevosDetectados = 0;

	preprocesamiento_imagen(_src, _dst);
	segmentacion_imagen(_dst, blobs, contadorDeBlobs);
	clasificacion_imagen(blobs, contadorDeBlobs, huevosDetectados, huevos);

	return huevosDetectados.to_int();
}
#else

int contar_huevos(TIPO_STREAM_DATOS& _src) {

	Blob blobs[CANTIDAD_MAXIMA_BLOBS];
	TIPO_REFERENCIA_A_BLOB contadorDeBlobs = 0;
	TIPO_REFERENCIA_A_BLOB huevosDetectados = 0;
	xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ> dst;

	preprocesamiento_imagen(_src, dst);
	segmentacion_imagen(dst, blobs, contadorDeBlobs);
	clasificacion_imagen(blobs, contadorDeBlobs, huevosDetectados);

	return huevosDetectados.to_int();
}

#endif

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "common/xf_common.h"
#include "common/xf_utility.h"
#include "constantes.h"
#include "ap_int.h"
#include "blob.h"
#include "camino.h"
#include "imgproc/xf_boundingbox.hpp"
#include "xf_headers.h"


int contar_huevos(TIPO_STREAM_DATOS& _src
#ifdef MODO_SIMULACION
		 , xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _dst,
		 Blob (&huevos)[CANTIDAD_MAXIMA_BLOBS]
#endif
);

#endif

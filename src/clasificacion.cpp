#include "clasificacion.h"
#include "ap_fixed.h"
#include "hls_math.h"


int clasificacion_imagen(
	Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS],
	TIPO_REFERENCIA_A_BLOB & contadorBlobs,
	TIPO_REFERENCIA_A_BLOB & huevosDetectados
#ifdef MODO_SIMULACION
		, Blob (&huevos)[CANTIDAD_MAXIMA_BLOBS]
#endif
){

	ap_ufixed<10,2> pi = 3.14;

	clasificacion_imagen_label0:for(int i=0; i<contadorBlobs; i++) {

		if(blobs[i].esValido){

			ap_fixed<45, 32> x_centroide = ap_fixed<45, 32>(blobs[i].momento10) / blobs[i].area;
			ap_fixed<45, 32> y_centroide = ap_fixed<45, 32>(blobs[i].momento01) / blobs[i].area;
			ap_fixed<45, 32> momentoCentral11 = blobs[i].momento11 - (x_centroide * blobs[i].momento01);
			ap_fixed<45, 32> momentoCentral20 = blobs[i].momento20 - (x_centroide * blobs[i].momento10);
			ap_fixed<45, 32> momentoCentral02 = blobs[i].momento02 - (y_centroide * blobs[i].momento01);

			int anchoBlob = blobs[i].columnaMaxima - blobs[i].columnaMinima;
			int altoBlob = blobs[i].filaMaxima - blobs[i].filaMinima;

			ap_fixed<45, 32> restaMomentos = (momentoCentral20 - momentoCentral02);
			ap_fixed<45, 34> numero = (restaMomentos * restaMomentos) + 4 * (momentoCentral11 * momentoCentral11);
			ap_fixed<45, 32> resultado = hls::sqrt(numero);

			ap_fixed<45, 32> a1 = (momentoCentral20 + momentoCentral02 + resultado);
			ap_fixed<45, 32> a2 = (momentoCentral20 + momentoCentral02 - resultado);

			ap_fixed<45, 32> lambda1 = 0;
			ap_fixed<45, 32> lambda2 = 0;
			ap_fixed<45, 32> ra = 0;
			ap_fixed<45, 32> rb = 0;
			ap_fixed<45, 32> areaElipseEquivalente = 0;
			ap_fixed<45, 32> excentricidad = 0;
			ap_fixed<45, 32> relacionDeArea = 0;

			if(blobs[i].area != 0 && a2 != 0){
				lambda1 = a1/2;
				lambda2 = a2/2;
				ra = 2 * hls::sqrt(lambda1/blobs[i].area);
				rb = 2 * hls::sqrt(lambda2/blobs[i].area);
				areaElipseEquivalente = pi * ra * rb;
				excentricidad = a1 / a2;
				relacionDeArea = areaElipseEquivalente / blobs[i].area;
			}

			if(anchoBlob != 0 &&
					altoBlob != 0 &&
					blobs[i].area.to_int() >= LIMITE_INFERIOR_AREA &&
					blobs[i].area.to_int() <= LIMITE_SUPERIOR_AREA &&
					relacionDeArea.to_float() > LIMITE_INFERIOR_RELACION_DE_AREA &&
					relacionDeArea.to_float() < LIMITE_SUPERIOR_RELACION_DE_AREA &&
					excentricidad.to_float() > LIMITE_INFERIOR_EXCENTRICIDAD &&
					excentricidad.to_float() < LIMITE_SUPERIOR_EXCENTRICIDAD){

				#ifdef MODO_SIMULACION
					huevos[huevosDetectados] = blobs[i];
				#endif

				huevosDetectados++;
			}
		}

	}

	return huevosDetectados;

}

#include "preprocesamiento.h"

void preprocesamiento_imagen(
		TIPO_STREAM_DATOS& _src,
		xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _dst){

	xf::Mat<XF_8UC3, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ> input;
	xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ> dst1;

	xf::AXIvideo2xfMat(_src, input);
	xf::rgb2gray(input, dst1);
	xf::Threshold<1, XF_8UC1, ALTURA, ANCHO>(dst1, _dst, UMBRAL, 255);
}

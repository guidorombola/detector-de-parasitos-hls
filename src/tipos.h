#ifndef _TIPOS_H_
#define _TIPOS_H_

#define TIPO_REFERENCIA_A_CAMINO ap_uint<9>
#define TIPO_REFERENCIA_A_BLOB ap_uint<10>
#define TIPO_AREA ap_uint<20>
#define TIPO_COORDENADA ap_uint<10>
#define TIPO_MOMENTOS_GEOMETRICOS ap_uint<32>
#define TIPO_STREAM_DATOS hls::stream< ap_axiu<24,1,1,1> >

#endif

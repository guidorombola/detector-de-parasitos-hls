#ifndef _CAMINO_H_
#define _CAMINO_H_

#include "tipos.h"

struct Camino{
	TIPO_COORDENADA inicio, fin;
	TIPO_REFERENCIA_A_BLOB referenciaABlob;
};

#endif

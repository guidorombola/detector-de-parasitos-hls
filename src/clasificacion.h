#ifndef _CLASIFICACION_H_
#define _CLASIFICACION_H_

#include "common/xf_common.h"
#include "common/xf_utility.h"
#include "blob.h"
#include "constantes.h"

#define LIMITE_INFERIOR_EXCENTRICIDAD 1.5
#define LIMITE_SUPERIOR_EXCENTRICIDAD 6

#define LIMITE_INFERIOR_RELACION_DE_AREA 1
#define LIMITE_SUPERIOR_RELACION_DE_AREA 1.06

#define LIMITE_INFERIOR_AREA 250
#define LIMITE_SUPERIOR_AREA 700

int clasificacion_imagen(
		Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS],
		TIPO_REFERENCIA_A_BLOB & contadorBlobs,
		TIPO_REFERENCIA_A_BLOB & huevosDetectados
#ifdef MODO_SIMULACION
		, Blob (&huevos)[CANTIDAD_MAXIMA_BLOBS]
#endif
);

#endif

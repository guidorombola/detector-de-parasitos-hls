#ifndef _PREPROCESAMIENTO_H
#define _PREPROCESAMIENTO_H

#include "common/xf_common.h"
#include "common/xf_utility.h"
#include "imgproc/xf_cvt_color.hpp"
#include "imgproc/xf_threshold.hpp"
#include "constantes.h"
#include "xf_headers.h"
#include "tipos.h"

void preprocesamiento_imagen(
		TIPO_STREAM_DATOS& _src,
		xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _dst);

#endif

#ifndef _SEGMENTACION_H
#define _SEGMENTACION_H

#include "common/xf_common.h"
#include "common/xf_utility.h"
#include "blob.h"
#include "camino.h"
#include "tipos.h"
#include "constantes.h"

void segmentacion_imagen(
		xf::Mat<XF_8UC1, ALTURA, ANCHO, PIXELES_POR_CICLO_RELOJ>& _src,
		Blob (&blobs)[CANTIDAD_MAXIMA_BLOBS],
		TIPO_REFERENCIA_A_BLOB &contadorDeBlobs);

#endif

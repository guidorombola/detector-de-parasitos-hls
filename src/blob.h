#ifndef _BLOB_H_
#define _BLOB_H_

#include "tipos.h"

struct Blob{

	TIPO_AREA area;

	TIPO_MOMENTOS_GEOMETRICOS momento10, momento01, momento02, momento20, momento11;

	TIPO_COORDENADA filaMinima, columnaMinima, filaMaxima, columnaMaxima;

	TIPO_REFERENCIA_A_BLOB referenciaANuevoBlob;

	bool esValido;

};


#endif

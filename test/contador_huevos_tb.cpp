#include <stdio.h>
#include <stdlib.h>

#include "../src/config.h"
#include "../src/blob.h"
#include "../src/xf_headers.h"

#ifdef _MSC_VER
	#include "dirent.h"
#else
	#include <dirent.h>
#endif

#include <iostream>

#include "test_bench.h"
#include "resultados_esperados.h"

using namespace std;

void dibujarBoundingRect(cv::Mat &imagen, Blob &huevo){

	cv::Rect_<int> roi(huevo.columnaMinima.to_int() - 5, huevo.filaMinima.to_int() - 5, huevo.columnaMaxima.to_int() - huevo.columnaMinima.to_int() + 10, huevo.filaMaxima.to_int() - huevo.filaMinima.to_int() + 10);
	cv::Scalar col = cv::Scalar(150);
	cv::rectangle(imagen, roi, col);

}

int procesar_imagen_hls(string nombre_imagen, string ruta_imagen) {
	char color = 1;

	cv::Mat in_img;
	cv::Mat out_img;

	in_img = cv::imread(ruta_imagen, color);

	if(in_img.data == NULL) {
		fprintf(stderr, "Cannot open image at %s\n", ruta_imagen.c_str());
		return 1;
	}

	static xf::Mat<XF_8UC1,ALTURA,ANCHO, PIXELES_POR_CICLO_RELOJ> imgOutput(in_img.rows,in_img.cols);
	TIPO_STREAM_DATOS _src;
	cvMat2AXIvideoxf<PIXELES_POR_CICLO_RELOJ>(in_img, _src);
	Blob huevosDetectados[CANTIDAD_MAXIMA_BLOBS];


	#ifdef MODO_SIMULACION
	int contadorDeHuevos = contar_huevos(_src, imgOutput, huevosDetectados);
	for(int i=0; i<contadorDeHuevos; i++){
		dibujarBoundingRect(in_img, huevosDetectados[i]);
	}
	string salida;
	salida.append(nombre_imagen.substr(0, nombre_imagen.length() - 4));

	cv::imwrite(("hls_" + salida + "_detecciones.bmp").c_str(), in_img);
	#else
		int contadorDeHuevos = contar_huevos(_src);
	#endif


	return contadorDeHuevos;
}

int procesar_imagenes(const char *path) {
	bool huboErrores = false;
	struct dirent *entry;
	DIR *dir = opendir(path);

	if (dir == NULL) {
		return 1;
	}

	while ((entry = readdir(dir)) != NULL) {

		string ruta_imagen(path);
		ruta_imagen.append("/");
		ruta_imagen.append(entry->d_name);

		if(strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
			int huevosDetectados = procesar_imagen_hls(entry->d_name, ruta_imagen);
			if(huevosDetectados != esperados[entry->d_name]){
				string nombreArchivoString = string(entry->d_name);
				huboErrores = true;
				printf("%s. Esperado: %d. Actual: %d\n", entry->d_name, esperados[nombreArchivoString], huevosDetectados);
			}
		}
	}

	if(huboErrores) {
		printf("Ejecucion no satisfactoria\n");
		return 1;
	}

	closedir(dir);
	return 0;
}

int main(int argc, char **argv) {
	if(argc != 2) {
		std::cerr << "Error: se esperaba un parametro correspondiente a la ruta de las imagenes\n";
		return 1;
	}
	string rutaBase = argv[1];
	return procesar_imagenes(rutaBase.c_str());
}


############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
set_directive_loop_tripcount -max 1024 -avg 300 "clasificacion_imagen/clasificacion_imagen_label0"
set_directive_loop_tripcount -min 1 -max 640 "incrementar_momentos/incrementar_momentos_label2"
set_directive_loop_tripcount -max 300 "calcular_momentos_centrales/calcular_momentos_centrales_label3"
set_directive_loop_tripcount -max 1024 -avg 300 "calcular_momentos_centrales/calcular_momentos_centrales_label4"
set_directive_loop_tripcount -max 640 -avg 64 "incrementar_momentos/incrementar_momentos_label5"
set_directive_loop_tripcount -max 300 "calcular_momentos_centrales_normalizados/calcular_momentos_centrales_normalizados_label6"
set_directive_loop_tripcount -min 480 -max 480 "detectar_blobs/detectar_blobs_label0"
set_directive_loop_tripcount -max 640 -avg 64 "verificar_solapamiento/verificar_solapamiento_label1"
set_directive_unroll -factor 8 "detectar_blobs/detectar_blobs_label0"
set_directive_unroll -factor 8 "inicializar_caminos/inicializar_caminos_label0"
set_directive_unroll -factor 8 "segmentacion_imagen/segmentacion_imagen_label0"
set_directive_unroll -factor 2 "incrementar_momentos/incrementar_momentos_label5"
set_directive_loop_tripcount -min 640 -max 640 "inicializar_caminos/inicializar_caminos_label0"
set_directive_dataflow "contar_huevos"
set_directive_interface -mode axis -register -register_mode both "contar_huevos" _src
set_directive_stream -depth 1 -dim 1 "contar_huevos" dst.data
set_directive_stream -depth 1 -dim 1 "preprocesamiento_imagen" dst1.data
set_directive_stream -depth 1 -dim 1 "preprocesamiento_imagen" dst2.data
set_directive_dataflow "preprocesamiento_imagen"
set_directive_stream -depth 1 -dim 1 "preprocesamiento_imagen" input
set_directive_loop_tripcount -max 30 "buscar_caminos_en_fila/buscar_caminos_en_fila_label2"
